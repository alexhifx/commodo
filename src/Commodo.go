/**
@author: varun<varunvasan91@gmail.com>
*/

package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"os/user"
	"syscall"
)

var (
	dir     string // Root directory for file server
	port    string // Port on which file server should run
	version bool   // Display version
	help    bool   // Display help
)

const (
	VERSION = "1.0"
)

func init() {
	flag.StringVar(&dir, "d", getHomeDir(), "The root directory for the file server.")
	flag.StringVar(&dir, "directory", getHomeDir(), "The root directory for the file server.")
	flag.StringVar(&port, "p", "4545", "The port on which the file server should run.")
	flag.StringVar(&port, "port", "4545", "The port on which the file server should run.")
	flag.BoolVar(&version, "v", false, "Prints the version number.")
	flag.BoolVar(&version, "version", false, "Prints the version number.")
	flag.BoolVar(&help, "h", false, "Prints the version number.")
	flag.BoolVar(&help, "help", false, "Prints the version number.")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: commodo [options]\n")
		fmt.Fprintf(os.Stderr, "Options:\n")
		fmt.Fprintf(os.Stderr, "\t-d, -directory  Directory   The root directory for the file server.\n")
		fmt.Fprintf(os.Stderr, "\t-p, -port       Port        The port on which the file server should run.\n")
		fmt.Fprintf(os.Stderr, "\t-v, -version    Version     Prints the version number.\n")
		fmt.Fprintf(os.Stderr, "\t-h, -help       Help        Show this help.\n")
	}

}

func showVersion() {
	fmt.Println("\nCommodo", VERSION)
	fmt.Println("This is a free software and comes with NO warranty.\n")
}

func main() {

	go handleExit() // goroutine to handle ctrl + c
	flag.Parse()    // parse command line arguments

	if version {
		showVersion()
		os.Exit(0)
	}

	if help {
		flag.Usage()
		os.Exit(0)
	}

	_, fileErr := os.Stat(dir)
	if fileErr != nil { // Check if path exists
		fmt.Println("Invalid Path `", dir, "`. Please specify a valid path.")
		os.Exit(1)
	}

	startServer() // start the file server
}

func startServer() {
	fmt.Printf("Starting Commodo with root %s.\nPress ctrl + c to exit.", dir)
	http.Handle("/", http.FileServer(http.Dir(dir)))
	conErr := http.ListenAndServe(":"+port, nil)
	if conErr != nil {
		fmt.Println(conErr)
		os.Exit(1)
	}
}

func getHomeDir() string {
	usr, err := user.Current()
	if err != nil {
		fmt.Println("Error getting user info.")
		os.Exit(1)
	}
	return usr.HomeDir
}

func handleExit() {
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt)
	for sig := range signalChannel {
		if sig == syscall.SIGINT {
			fmt.Println("\nCommodo File server stopped.")
			os.Exit(0)
		}
	}
}
